---
title: Articles of Association
layout: page
menu_active: Documents
---

<p><em>As a non-profit corporation incorporated under German law, KDE e.V. is
required to declare its intentions and by-laws in a formal "Articles of
Association" document. The original and legally binding version is the German
version, the <a href="/corporate/statutes-de/">"Satzung"</a> (<a
href="/corporate/satzung-kde-ev.pdf">PDF</a>). The following translation is
for information only.</em></p>

<h2>Clarification of Terminology</h2>

<ol>
  <li>Under these Articles of Association we consider computer programs as
  free software, or &quot;open source software&quot;, which the
  originator puts into the public domain free of charge and in an irretrievable
  manner. In doing so, the originator allows others the freedom to use the
  program for any purpose, to study how the program functions, to adapt it to
  his/her own needs, to copy it for others and to improve the program and make
  these changes available for the common good.</li>

  <li>Developing free software in the sense of these Articles of
  Association consists of research and compilation of basic principles and
  concepts as well as their test by programming and test of free
  software which implement such concepts and basic principles.</li>

  <li>&quot;K Desktop Environment (KDE)&quot; is a graphic work environment
  containing not only the infrastructure for programs but also user programs,
  including sounds, pictures, documents and translations. It is being developed
  as free software and is made available to the general public free
  of charge.</li>
</ol>

<a name="2"></a>
<h2>1. Company Name, Registered Office, Financial Year</h2>

<ol>
  <li>The Association is registered as <i>K Desktop Environment e.V.</i>,
  abbreviation <i>KDE e.V.</i></li>

  <li>The Association’s registered offices are located in Berlin. Should
  no fixed registered office be established, it will be managed from the
  residence of the respective Board member acting as general manager at any
  given time.</li>

  <li>It is listed under register number 31685 in the Charlottenburg District
  Court’s Register of Associations.</li>

  <li>The financial year is the calendar year.</li>
</ol>

<a name="2"></a>
<h2>2. Purpose of the Association</h2>
<ol>
  <li>The Association’s purpose is the promotion and distribution of free
  desktop software in terms of free software, and the program package &quot;K
  Desktop Environment (KDE)&quot; in particular, to promote the free exchange
  of knowledge and equality of opportunity in accessing software as well as
  education, science and research.</li>

  <li>The aim of the articles of association is to promote
    <ol style="list-style-type:lower-alpha">
      <li>education, exchange of opinions and cooperation between users,
      developers and researchers</li>

      <li>research and debate on the impact of free desktop software
      and the notion of free software on society and science</li>

      <li>continuous development and research into free desktop
      software</li>

      <li>enhanced access to free desktop software, supporting
      pictures, sounds, data and documentation and the compilation and
      distribution of relevant material</li>

      <li>contributions and articles providing competent information to the
      public within the association’s field of activity and presence at trade
      fares and congresses, making information available to a broad spectrum of
      users.</li>

      <li>organisation of congresses and lectures open to the general public,
      which aim at promoting further training of project members and users</li>

      <li>protection of the project members free rights protecting them from
      the commercial interests of third parties</li>
    </ol>
  </li>

  <li>Amendment of the association’s purpose may only be made in accordance
  with § 3 (1).</li>

</ol>

<a name="3"></a>
<h2>3. Non-profit Association</h2>

<ol>
  <li>The activities of the Association are exclusively and directly limited to
  non-profit activities as stipulated in § 2 of the Articles of Association and
  to tax-privileged purposes in accordance with §§ 51ff. AO (German Tax
  Regulations). Its activities are for the common good and are not pursued
  primarily for the purpose of profit making.</li>

  <li>The Association’s financial means may only be used for purposes
  stipulated in the Articles of Association. Members will only be reimbursed
  for their expenses but will not receive any direct benefits out of the
  Association’s funds.</li>

  <li>It is not permitted to favour somebody through association expenditures,
  which do not serve the associations purpose or are disproportionately high.
  Reimbursements for expenditures are made in accordance with the Federal
  Travel Expenses Code should no other legal regulations apply.</li>
</ol>

<a name="4"></a>
<h2>4. Types of Membership and Members of the Association</h2>

<ol>
  <li>Both natural persons and legal bodies wanting to implement and promote
  the aims of the association may become members. Taking into account the
  international character of the Association and to give members the
  possibility to stop actively supporting and developing KDE without
  necessarily losing their membership, the following types of memberships are
  available:

  <ol style="list-style-type:lower-alpha">
    <li><b>Active members</b> are natural persons supporting the Association’s
    purpose and aims through their cooperation and who in doing so take over
    the complete duties of an association member. Primarily, they are expected
    to cooperate, participate in the general assembly and exercise their voting
    rights.</li>

    <li><b>Extraordinary members </b>are natural persons and legal bodies, who
    through their membership declare their support of the Association’s
    purposes and aims, while refraining from voting rights and the exertion of
    the active members’ right to participate in the general assemblies. Legal
    bodies chose a representative to exercise the remaining rights and
    duties.</li>

    <li><b>Supporting Members</b> are extraordinary members supporting the
    Association’s
    purposes and aims primarily through financial or material contributions. If
    desired, their names will be published on the Association’s website, and
    they have the right to participate in the general meetings. They do not
    have any voting rights.</li>
  </ol></li>

  <li><p>An active membership will be granted if suggested by an active member and
  supported by two other active members, if the general meeting decides to
  grant it or if a simple majority of the active members is obtained by means
  of an internet vote. The main criterion for granting membership should be the
  candidate’s commitment over a longer period of time and the contributions
  he/she made in order to fulfil the Association’s aims.</p>

  <p>An active member can change status to that of an extraordinary member by
  submitting his request to the Board. Should an active member not fulfil the
  above mentioned duties over a period of two consecutive general meetings, the
  membership will automatically change into an extraordinary membership. An
  extraordinary member, but not a supporting member, can submit an application to become
  an active member to the Board. The Board decides whether or not to accept a
  supporting member.</p></li>

  <li>The membership terminates due to exit, expulsion, death, or – in the case
  of legal bodies – due to the loss of its legal status. The membership may be
  terminated on December 31<sup>st</sup> of each year, and a written request
  has to be submitted to the Board at least 4 weeks prior.</li>

  <li><p>The expulsion of a member is reserved for critical situations, and
  generally it will be considered preferable to find a peaceful solution. The
  expulsion becomes effective immediately after the Board’s decision. The
  following reasons may cause an expulsion:</p>

  <ol type="a">
    <li>if a member seriously violates regulations contained in the Articles of
    Association and/or the purpose and aim of the Association, and an attempt
    to clarify the situation was unsuccessful; and</li>

    <li>if the membership fees are not paid over a period of 12 months despite
    repeated reminders.</li>
  </ol>

  <p>Before such a decision will be taken, the member must have the opportunity
  to justify or explain his/her actions. An objection to such an expulsion may
  be filed with the Board within four weeks and will be voted on during the
  next general meeting. Until such decision will be taken by the general
  meeting, the member’s rights and duties remain dormant.</p></li>

  <li>If a member leaves the Association or the Association is liquidated, no
  member has a right of reimbursement for the assets he/she may have
  contributed.</li>
</ol>

<a name="5"></a>
<h2>5. Organs of the Association</h2>

<p>The organs of the Association are:</p>
<ul>
  <li>the General Assembly and</li>
  <li>the Board.</li>
</ul>

<a name="6"></a>
<h2>6. General Assembly</h2>

<ol>
  <li>The General Assembly consists of all of the Association’s active members,
  who have one vote.</li>

  <li>The General Assembly takes place at least once a year. The Board invites
  the members per mail or email, providing a preliminary agenda. The invitation
  has to be sent at least six weeks prior to the meeting. The time limit starts
  on the day after the invitation letter was sent. The date of the post mark
  and the date when the email was sent are binding. The invitation letter is
  considered as delivered, if it was addressed to the last address the member
  advised in writing. The members may submit additional agenda items to the
  board either in writing or electronically until two weeks before the assembly.
  The date when the letter or email
  was received is binding. The Board will publish the final agenda in the
  internet, and the address is to be mentioned in the written invitation. </li>

  <li>If it is in the Association’s interest, an extraordinary General Assembly
  may be called for. If this request is submitted to the Board in written form
  by at least 20% of the members, the Board is compelled to call for an
  extraordinary General Assembly within six weeks. The member’s request must
  contain the desired agenda item.</li>

  <li>Independent from the number of members present, the General Assembly only
  has a quorum if the invitation was submitted in proper form. It nominates one
  of the members present as head of the General Assembly. Unless otherwise
  decided by the General Assembly, all decisions will be taken openly by show
  of hands and with a simple majority. Should the number of pro and contra
  votes should be equal, the application is considered as dismissed.</li>

  <li>Differing from (4), a majority of ¾ of the votes of the General Assembly
  and a minimum of half of the votes of all active members are necessary to
  amend the Articles of Association or decide the liquidation of the
  Association.</li>

  <li>An active member who cannot personally participate in the General
  Assembly may ask another member, who will be personally present, to represent
  him during the General Assembly. The representative will exercise both
  members’ voting rights. The representative will legitimise himself at the
  beginning of the General Assembly by presenting to the Board the original
  of a written authorisation. A representative may represent a maximum of two
  additional members.</li>
</ol>

<a name="7"></a>
<h2>7. Duties of the General Assembly</h2>

<ol>
  <li><p>The General Assembly is the highest decision making organ of the
  Association and is generally in charge of all duties, unless these Articles
  of Association have assigned specific duties to another organ of the
  Association.</p>
  <p>The General Assembly elects the board from a number of active members. The
  people obtaining the largest numbers of votes will be elected. Ballot papers will be
  provided to cast the votes.</p></li>

  <li>The General Assembly may deselect a board member. Differing from (1),
  this process requires the majority of votes of all active members.</li>

  <li>The General Assembly decides about the objections of members the Board
  intends to expel.</li>

  <li>The General Assembly receives the Board’s annual report as well as the
  audit report of the auditor and discharges the board’s responsibilities.</li>

  <li>The right to decide about amendments to the Articles of Association or a
  liquidation of the association is reserved to the General Assembly.</li>

  <li>Namely the annual accounts and the annual report have to be presented in
  written format to the General Assembly for decision making and discharge of
  the board. It calls on two auditors, who belong neither to the board nor any
  other panel appointed by the board and who are not employed by the
  association, in order to audit the accounting and annual report and relate
  their results to the General Assembly. The auditors have access to all of the
  Association’s accounting documents.</li>

  <li>Furthermore the General Assembly has the exclusive right to decide
  about:

  <ol style="list-style-type:lower-alpha">
    <li>any purchase of, sale of or charges on real property,</li>
    <li>any investment in other companies and</li>
    <li>raising loans of EUR 10,000 and more</li>
  </ol></li>

  <li>It has the right to decide about all matters board or members submit to
  it.</li>
</ol>

<a name="8"></a>
<h2>8. The Board</h2>

<ol>
  <li>The Board consists of 5 persons, and only natural persons can be board
  members. They are elected for a 3 years term. Re-election is permitted. The
  respective board members remain in office after expiration of their term
  until a successor is elected.</li>

  <li>From its midst the Board elects a chairman and two proxies. One of the
  proxies is the treasurer. Re-election is permitted.</li>

  <li>The Board’s duties, namely all regulations pertaining to calling for
  board meetings, meeting procedures and voting process are stipulated in the
  by-laws of the board, which it has to decide unanimously.</li>

  <li>The board decides about all matters pertaining to the association, unless
  a decision of the General Assembly is required. It implements the decisions
  taken by the General Assembly.</li>

  <li>Each individual board member has the right to represent the Association
  to outside parties.</li>

  <li>Should a board member quit while in office, the board appoints a
  provisional board member on the basis of an internet vote in which all active
  members participate. The provisional board member remains in office until the
  General Assembly takes place. The General Assembly decides about the definite
  succession within the board.</li>

  <li>In accordance with §30 BGB (German Civil Code) the board can decide to
  appoint a full-time director as a special representative, who manages all of
  the Association’s current affairs and acts as supervisor of the Association’s
  full-time employees. Decisions pertaining to work contracts, dismissal and
  admission of new members may only be taken by the board.</li>

  <li>The director is committed to participate in the General Assemblies and
  has the right or duty (if the board demands it) to participate in the board
  meetings. He has the right of speech in all meetings and is accountable to
  all organs of the association.</li>

  <li>Amendments to the Articles of Association demanded by regulatory, court
  or financial authorities for formal reasons may be undertaken by the board.
  The General Assembly has to be informed of such amendments in its next
  meeting.</li>
</ol>

<a name="9"></a>
<h2>9. By-Laws</h2>

<p>The by-laws govern all details of the Associations activities which are not
covered in the Articles of Association. The Board implements the by-laws following
the members approval. The members' approval is obtained by means of an internet
vote and a simple majority of the active members.</p>

<a name="10"></a>
<h2>10. Meeting Minutes</h2>

<p>All decisions taken by the Board and General Assembly will be documented in
writing and are available for the members' perusal. The minutes are signed
by the chairperson and the taker of the minutes.</p>

<a name="11"></a>
<h2>11. Labour Agreements</h2>

<p>Before hiring a full-time employee the board will regulate the remuneration
in the by-laws.</p>

<a name="13"></a>
<h2>13. Financing of the Association</h2>

<ol>
  <li>The funds required for the Association are raised through
  <ol style="list-style-type:lower-alpha">
    <li>membership fees,</li>
    <li>contributions by the federal states, town councils and other public
    bodies,</li>
    <li>donations, </li>
    <li>other financial contributions made by third parties,</li>
    <li>benefits paid to the Association for activities it pursues for the
    common good, including lectures.</li>
  </ol></li>

  <li>The members pay fees based on the decision taken by the General Assembly
  or by the active members in an internet vote with a simple majority. Once
  defined the membership fees will be incorporated in the by-laws.</li>

<li>In case of a dissolution or liquidation of the association or termination
of its tax-privileged status all its assets will become property of Deutsche
Unesco-Kommision e.V. , Colmantstraße 15, D-53115 Bonn, which has to use them
exclusively and directly for non-for-profit purposes only according the aims of
this articles.</li>
</ol>

<a name="14"></a>
<h2>14. Inception of the Articles of Association</h2>

<p>These Articles of Association came into effect following a decision by the
General Assembly and replace the Articles of Association effective since
November 26, 1997.</p>

<p>Nove Hrady, Czech Republic, August 22, 2003 </p>

<p><em>Section 8.1 changed by decision of the general assembly at Dublin, Ireland,
on September 26th 2006.</em></p>

<p><em>Section 10 changed by decision of the general assembly at Las
Palmas, Gran Canaria, on July 7th 2009.</em></p>

<p><em>Section 1.2 changed by decision of the general assembly at Tampere,
Finland, on July 5th 2010.</em></p>
