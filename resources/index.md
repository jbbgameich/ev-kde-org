---
title: "Forms"
layout: page
---

+ [Application for Supporting Membership](supporting_member_application.pdf). 
  Fill out this form
  when applying for a corporate supporting membership.
+ [New Member Questionnaire](ev-questionnaire.text). 
  Ordinary
  members have to answer this questionaire when applying for membership in the
  KDE e.V. Please note that your membership has to be proposed by an existing
  member to the membership.
+ *Expense Report Forms*. Please fill out this 
  form when handing in receipts for costs the board has accepted to reimburse 
  you for. These forms do **not** apply for trips to sprints, conferences, etc. those
  are governed by the
  [Travel Cost Reimbursement Policy](/rules/reimbursement_policy/).
  * For reimbursement 
    [via PayPal](expense-paypal.pdf), use the 
    [PayPal form](expense-paypal.pdf).
  * For reimbursement 
    [via international bank transfer](expense-iban.pdf), use the 
    [international bank transfer (IBAN) form](expense-iban.pdf).
  * For reimbursement via other means or if you do not understand which of the above forms to use, use the 
    [traditional expense report form](expense_report.pdf).
+ <a href="proxy_instructions.pdf">Proxy Instructions</a>. If you are an
  active member of the KDE e.V. and are unable to attend the general assembly
  you can give a proxy the right to vote for you. Fill out this form and make
  sure the chairman of the general assembly has received it before the beginning
  of the assembly.
+ <a href="vereinfachter_zuwendungsnachweis_update2019.pdf">"Vereinfachter
  Zuwendungsnachweis".</a> If you are donating money to the KDE e.V., you can
  use this form to make use of the German tax-exemption rules. For more 
  information, please refer to <a href="/donations-taxes-de/">this page</a>
  (in German).</li>
+ Fiduciary Licensing Documents. The FLA can *optionally* be used to assign the
  copyright in your contributions to KDE to KDE e.V.:
    <ul>
      <li><a href="FLA.pdf" title="FLA">FLA</a> (Fiduciary License Agreement)</li>
      <li>The <a href="FRP.pdf" title="FRP text">FRP</a> is used as a document to direct how (potential) relicensing would occur.</li>
    </ul>
  <a href="/rules/fla/">General information</a> about the FLA is <a href="/rules/fla/">available here</a>.
+ <a href="local_kde_org_agreement.pdf">Agreement about local KDE
  organization</a>. This agreement is used between KDE e.V. and a local
  organization representing KDE in a specific region. It grants the local
  organization the limited right to represent KDE locally and use the KDE
  trademark for that purpose. It's required that the local organization follows
  KDE e.V.'s goals and non-profit rules.
