---
title: "KDE e.V. System Administration Working Group"
layout: page
---

### Goals

The System Administration Working Group is responsible for the administration
of the KDE server infrastructure.

### Rules

> The Sysadmin Working Group has fairly stringent membership requirements
> but these are not documented here.

### Members

+ Ben Cooksley
+ Bhushan Shah
+ Kenny Coyle
+ Nicolás Alvarez

### Contact

You can reach the System Administration Working Group by mail at
[sysadmin@kde.org](mailto:sysadmin@kde.org).
