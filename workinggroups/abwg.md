---
title: "KDE e.V. Advisory Board Working Group"
layout: page
---

### Goals

The Advisory Board Working Group has the following tasks:

+ Be available as contacts for questions and feedback from the Advisory Board members.
+ Take feedback and questions from the Advisory Board into the wider KDE community.
+ Organize Advisory Board meetings.

### Rules

> The Advisory Board Working Group has no specific rules.

### Members

The Advisory Board Working Group consist of one volunteer contact person for each Advisory Board member organization. The current members are:

+ Kai Uwe Broulik
+ Tomaz Canabrava
+ David Edmundson
+ Eike Hein
+ Lydia Pintscher
+ Aleix Pol
+ Cornelius Schumacher

### Contact

You can contact the Advisory Board Working Group under 
[advisory-board-contacts@kde.org](mailto:advisory-board-contacts@kde.org).
