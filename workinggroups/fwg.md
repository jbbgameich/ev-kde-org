---
title: "KDE e.V. Financial Working Group"
layout: page
---

### Goals

The Financial Working Group has the following goals:

+ Support KDE e.V. Board in Financial topics.
+ In collaboration with the Treasurer, propose financial decisions to the KDE e.V. Board.
+ Support Board members in commercial activities.
+ Support the Treasurer in communication actions related to financial topics.
+ Evaluate the regular financial reports done by the Treasurer.

### Rules

The members are selected by the KDE e.V. for a two-year period.

The KDE e.V. board member who vacates the position of treasurer at the end of a regular duty cycle on the board automatically joins the Financial Working Group membership for a two-year period.

The Financial Working Group members cannot be selected as auditors of accounting.

Members can step down from the Financial Working Group at any time.

### Members

+ David Edmundson david@davidedmundson.co.uk (May 19th, 2019)
+ Daniele E. Domenichelli ddomenichelli@drdanz.it (May 19th, 2019)
+ Neofytos Kolokotronis neofytosk@posteo.net (May 19th, 2019)

### Contact

[kde-ev-financial-wg@kde.org](mailto:kde-ev-financial-wg@kde.org)
